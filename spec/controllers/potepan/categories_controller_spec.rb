require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do

  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    before { get :show, params: { id: taxon.id } }

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "instance value is correct" do
      expect(assigns(:taxon)).to eq taxon
    end
  end

end
