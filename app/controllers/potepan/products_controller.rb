class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCTS_COUNT = 8
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.includes(:taxons).related_products(@product).where.not(id: @product).limit(MAX_RELATED_PRODUCTS_COUNT).uniq
  end
end
