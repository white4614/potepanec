require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do

  describe "GET #show" do
    context "test for instance selected product" do
      let(:product) { create(:product) }
      before { get :show, params: { id: product.id } }

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "instance value is correct" do
        expect(assigns(:product)).to eq product
      end
    end

    context "test for related products" do
      let(:categories)  { create(:taxonomy, name: 'Categories') }
      let(:brands)      { create(:taxonomy, name: 'Brands') }
      let(:bags)        { create(:taxon, name: 'Bags', taxonomy: categories) }
      let(:mugs)        { create(:taxon, name: 'Mugs', taxonomy: categories) }
      let(:ruby)        { create(:taxon, name: 'Ruby', taxonomy: brands) }
      let(:apache)      { create(:taxon, name: 'Apache', taxonomy: brands) }

      let(:ruby_bags)   { create_list(:product, 3, taxons: [bags, ruby]) }
      let(:apache_bags) { create_list(:product, 3, taxons: [bags, apache]) }
      let(:ruby_mugs)   { create_list(:product, 3, taxons: [mugs, ruby]) }
      let(:apache_mugs) { create_list(:product, 6, taxons: [mugs, apache]) }

      let(:max_related_count) { 8 }

      it "related products is correct" do
        ruby_bag = ruby_bags.first
        related_products = ruby_bags + ruby_mugs + apache_bags - [ruby_bag]
        get :show, params: { id: ruby_bag.id }
        expect(assigns(:related_products)).to match_array related_products
      end

      it "related products is not more than 9" do
        apache_mug = apache_mugs.first
        get :show, params: { id: apache_mug.id }
        expect(assigns(:related_products).count).to be <= max_related_count
      end
    end
  end
end
