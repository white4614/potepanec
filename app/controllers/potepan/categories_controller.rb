class Potepan::CategoriesController < ApplicationController

  def show
    @taxon = Spree::Taxon.includes(products: [master: [:default_price, :images]]).find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(taxons: :products)
  end
end
