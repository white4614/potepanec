class Potepan::HomeController < ApplicationController
  NEW_ARRIVALS_DISPLAY = 8
  def index
    @new_arrivals = Spree::Product
                      .includes_price_and_image
                      .order(available_on: :desc)
                      .limit(NEW_ARRIVALS_DISPLAY)
  end
end
