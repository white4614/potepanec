Spree::Product.class_eval do
  scope :related_products, ->(product) { includes_price_and_image.where(spree_taxons: { id: product.taxons.ids }) }
  scope :includes_price_and_image, -> { includes(master: [:default_price, :images]) }
end
