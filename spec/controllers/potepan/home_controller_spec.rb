require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do

  describe "GET #index" do
    before { get :index }
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    context "test for new arrivals" do
      let(:max_new_arrivals_count) { 8 }
      let(:new_products) { create_list(:product, max_new_arrivals_count, name: 'new_product', available_on: 1.hour.ago) }
      let(:old_products) { create_list(:product, 3, name: 'old_product', available_on: 2.hours.ago) }

      it "displays correct new arrivals" do
        expect(assigns(:new_arrivals)).to match_array new_products
      end

      it "displays correct count of new arrivals" do
        expect(assigns(:new_arrivals).count).to be <= max_new_arrivals_count
      end

      it "doesn't display old products" do
        expect(assigns(:new_arrivals)).not_to include old_products
      end
    end
  end

end
